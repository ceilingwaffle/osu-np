require("dotenv").config();
const tmi = require("tmi.js");
const fs = require("fs");

let opts = {
  identity: {
    username: process.env.TWITCH_BOT_USERNAME,
    password: process.env.TWITCH_BOT_OAUTH_PASSWORD,
  },
  channels: [process.env.TWITCH_TARGET_CHANNEL],
};

console.log(`[${getNow()}] Starting osu! np bot...`);

// Helper function to send the correct type of message:
function sendMessage(target, context, message) {
  if (context["message-type"] === "whisper") {
    client.whisper(target, message);
  } else {
    client.say(target, message);
  }
}

// Create a client with our options:
let client = new tmi.client(opts);

// Register our event handlers (defined below):
client.on("message", onMessageHandler);
client.on("connected", onConnectedHandler);
client.on("disconnected", onDisconnectedHandler);

// Connect to Twitch:
client.connect();

// Called every time a message comes in:
function onMessageHandler(target, context, msg, self) {
  const commands = getCommands().map((c) => c.trim().toLowerCase());

  if (commands.includes(msg.trim().toLowerCase())) {
    fs.readFile(process.env.OSU_NP_TEXT_FILE_PATH, "utf8", function (err, contents) {
      if (err) {
        logError(err);
        return;
      }

      sendMessage(target, context, contents);
      console.log(`[${getNow()}] Responded to !np from ${target}: ${contents}`);
    });
  }
}

// Called every time the bot connects to Twitch chat:
function onConnectedHandler(addr, port) {
  console.log(`* Connected to ${addr}:${port}`);
}

// Called every time the bot disconnects from Twitch:
function onDisconnectedHandler(reason) {
  console.log(`Disconnected: ${reason}`);
  process.exit(1);
}

function getNow() {
  return new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
}

function logError(err) {
  console.log(`[${getNow()}] ${err}`);
}

function getCommands() {
  const commandsString = process.env.COMMANDS;

  if (!commandsString || !commandsString.length || !commandsString.trim().length) {
    return ["!np"];
  }

  const commands = commandsString.split(",");

  if (!commands.length) {
    return ["!np"];
  }

  return commands;
}
