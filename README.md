# About
This is a Twitch chat bot to respond to every Twitch !np message with the contents of a text file (created by something like osu! Stream Companion).

# Installation
- Install NodeJS from https://nodejs.org/en/download/
- Download the latest release of this repository from: https://bitbucket.org/ceilingwaffle/osu-np/get/HEAD.zip
- Extract the zip file to a folder somewhere
- Press win+R to open the run window
- type: cmd
- In the console window, type:
```
cd <folder path where you extracted the zip file>
```
- Install the required NodeJS modules:
```
npm install
```
- Rename the file .env.example to .env
- Open .env in notepad and modify the Twitch username, channel, oauth password, and osu stream companion txt file location (Get your oauth password from https://twitchapps.com/tmi)
```
TWITCH_BOT_USERNAME=my_twitch_username
TWITCH_TARGET_CHANNEL=my_twitch_channel_name
TWITCH_BOT_OAUTH_PASSWORD=oauth:4MvU3ZyueA0gYgQ9hVKwOo
OSU_NP_TEXT_FILE_PATH=C:\Program Files (x86)\StreamCompanion\Files\np.txt
```

# Running
- Start the bot by typing this in the cmd window:
```
npm start
```